﻿#pragma strict
import UnityEngine.UI;

var rb: Rigidbody;
var countText : GUIText;
var winText: GUIText;

var count: Number;
var speed: float;

function Start () {
	rb = GetComponent.<Rigidbody>();
	speed = 300;
	count = 0;
	setCount();
	winText.gameObject.SetActive( false );
}

function FixedUpdate () {
	var h : float = Input.GetAxis('Horizontal');
	var v : float = Input.GetAxis('Vertical');

	var movement : Vector3 = Vector3(h , 0, v);
	rb.AddForce( movement * speed * Time.deltaTime );
}

function OnTriggerEnter( other : Collider){
	if( other.gameObject.CompareTag('pick up')){
		other.gameObject.SetActive( false );
		count++;
		setCount();
		if( count >= 12 ){
			winText.gameObject.SetActive( true );
		}
	}
}

function setCount(){
	countText.text = 'Count:' + count;
}