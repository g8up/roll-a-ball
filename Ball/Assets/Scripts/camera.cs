﻿using UnityEngine;
using System.Collections;

public class camera : MonoBehaviour {
	public GameObject player;
	private Vector3 offset;

	// Use this for initialization
	void Start () {
		offset = transform.position;
	}
	
	// Update is called once per frame
	void LateUpdate () {
		// 跟随 Player
		transform.position = player.transform.position + offset;
	}
}
